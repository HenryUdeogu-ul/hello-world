# Hello World

Tutorial for learning GIt A#1

Changes made on README.md using atom editor.

Added an author Henry Udeogu

# Project Title

MMAS (Multi Module Authentication System)

# Description

MMAS uses multiple modules for human recognition (facial images, audio, biometrics, retina scan, random generated individual key code, RFID card). The security features can be varied depending on the authentication modules selected by the administrator.

# Prerequisites

Install Anaconda, OpenCV, Arduino Uno software IDE.
